---
layout: default
permalink: meetup/0x01/
---

# [0x01] - [MOBILE] Не безопасность Android/iOS  

- Что:		Встреча 0x01 была посвящена безопасности Android/iOS. Методам проектирования, способам тестирования и анализа приложений.
- Где и когда: [ссылка на meetup.com](https://www.meetup.com/DC8422/events/239977688/)

## Доклады встречи
- [Доклад 0x00. {Android} Почему не стоит доверять свои данные Android аппаратам.](#report0x00)
- [Доклад 0x01. {Android/iOS} Certificate pinning: как не выстрелить себе в ногу.](#report0x01)
- [Доклад 0x02. {Android/iOS} Построение antifraud систем защиты и систем определения читеров и как быть таковым, но не попасться.](#report0x02)

### <a name="report0x00"></a>**Доклад 0x00. Почему не стоит доверять свои данные Android аппаратам.**

#### Тезисы

 - Устройство Andoid на пальцах;
 - Обзор способов защиты от несанкционированного доступа, доступных "из коробки". Взгляд изнутри;
 - Векторы? (способы) доступа к информации (ADB, JTAG, BootRom, Прямое чтение накопителя);
 - Демонстрация;

#### YouTube: [https://www.youtube.com/watch?v=w4Mw9hHeivI](https://www.youtube.com/watch?v=w4Mw9hHeivI)

#### Спикер: Evgeny [RusEm] Kazaev (Реверсер на вольных хлебах)

### <a name="report0x01"></a>**Доклад 0x01. Certificate pinning: как не выстрелить себе в ногу.**

#### Тезисы

 - Что такое Certificate Pinning, какие виды бывают;
 - Для чего это нужно в мобильных приложениях;
 - Существующие открытые реализации (библиотеки); 
 - Какие подводные камни существуют;

#### YouTube: [https://www.youtube.com/watch?v=urGSJSBjHV8](https://www.youtube.com/watch?v=urGSJSBjHV8)

#### Спикер: Wire Snark (co-founder @ DEF CON Nizhny Novgorod)

### <a name="report0x02"></a>**Доклад 0x02. Построение antifraud систем защиты и систем определения читеров и как быть таковым, но не попасться.**

#### Тезисы

 - Jailbreak? Root? Xposed?
 - Что смотреть, на что обращать внимание и как ловить читеров\хакеров\ботов\etc или как быть таковым, но оставаться "в тени";
 - Пример обхода antifraud-системы и способы ее усовершенствования;
 - Как использовать эти знания и что с ними делать;

#### YouTube: [https://www.youtube.com/watch?v=x3zQ3N-lF2M](https://www.youtube.com/watch?v=x3zQ3N-lF2M)

#### Спикер: [Roman [deadroot] Ananev](https://t.me/deadroot)

### **После встречи круглый стол и вылазка в бар**

Обсуждались IPv6 & GNU.

## P.S.
- Го заполнять форму  [https://goo.gl/forms/dwM8q1SpUjX7EMEn2](https://goo.gl/forms/dwM8q1SpUjX7EMEn2) :)<br>
- Чтобы стать докладчиком с крутой темой просто напиши в [https://t.me/dc20e6](https://t.me/dc20e6)
