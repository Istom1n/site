---
layout: post
title: NEWS. DefCon теперь и в Ульяновске
date: 2017-04-15 00:00:00
categories: news
short_description: Привет, Ульяновск. Мы решили и тут продвинать инфосек, а то что как эти?
image_preview: https://avatars1.githubusercontent.com/u/27823111?v=3&s=30
---

Как-то на [Cтачке](https://nastachku.ru) (на самом деле раньше, когда только началась формироваться секция [информационной безопасности](https://nastachku.ru/infosec) для этой конференции) встретилась группа людей и поняла что в Ульяновске надо создавать и развивать сообщество, посвященное информационной безопасности. А вот пока ничего лучше кроме DEFCON сообществ не найдено, да еще и можно сделать это круто и открыто для всех, обучая и преподнося информацию всем, кто хочет ее внимать.

Так и родилась эта идея. Ну что, надо начинать и делать.

Сначала надо было провести Стачку и [сделали это круто](http://telegra.ph/STACHKA2017-InfoSec-04-15), вложившись полностью и сделав реально крутые секции по [инфосеку](https://nastachku.ru/infosec "[Simtech Development] Roman Ananev") и [менеджменту](https://nastachku.ru/management "[Simtech Development] Damiil Bazenov"), но надо вперед и расти и развиваться.

Паралельно со Стачкой мы запилили [квест](https://quest.simtech.ru), чтобы понять насколько сильно жаждет учиться и обмениваться знаниями или же не готово местное сообщество к такому. Если честно сказать -- мы были прятно удивлены, так как 50 человек начало проходить в 1 день и потос к ним еще 10-15 присоединилось во второй. Да, многие бросили на половине пути, встретивштсь с трудностями, но от этого было даже интереснее тем, кто [проходил до конца](https://quest.imba.wtf/top.php?quest=stachka2017).

На конференции мы сделали [чат в Telegram](https://t.me/dc20e6), в котором и спросили: "Надо ли сообщество такое и готовы ли учиться, тратить силы, вкладываться в это во имя обще идеи и синергии знаний?". На что услышали положительный ответ в подтверждение своей идеи и приступили к реализации.