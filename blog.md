---
layout: default
permalink: blog/
---

<section>
  <h1>Блог</h1>

  <p>
    На этой странице будет информация о наших встречах, полезная информация о проектах, которые мы делаем. Так же здесь будут видео с выступлений :)
  </p>
</section>

<section>
  <h2>Посты, встречи м прочие полезности</h2>

  {% for post in site.posts %}
    <div class="media t-hackcss-media">
      <div class="media-body">
        <div class="media-heading">
          <span>{{ post.date | date_to_string }} &raquo;
            {% if post.external_url %}
              <a href="{{ post.external_url }}" target="_blank" title="{{ project.name }}">
                {{ post.title }}
              </a>
            {% else %}
              <a href="{{ post.url | prepend: site.baseurl }}" title="{{ project.name }}">
                {{ post.title }}
              </a>
            {% endif %}
          </span>
        </div>
        <div class="media-content">
          {{ post.short_description }}
        </div>
      </div>
    </div>
  {% endfor %}
</section>
