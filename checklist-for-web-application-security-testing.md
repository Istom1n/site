---
layout: default
excerpt: Чеклист для проведения тестирования безопасности web приложения
permalink: checklist-for-web-application-security-testing/
---

# Чеклист для проведения тестирования безопасности web приложения

> **TL;DR;** Да, мы знаем что все стремится к упрощению и покрытию одними тулзами других, так как лень :) Поэтому сейчас перечислим различные мегаметасплоиты, запускалки других запускалок, а потом распишем по шагам.
> 
> - [Sn1per](https://github.com/1N3/Sn1per) - is an automated scanner that can be used during a penetration test to enumerate and scan for vulnerabilities

---

- [**1. Разведка / OSINT**](#1)
    - [Поиск персонала](#ppl-osint)
    - [Сканирование портов](#port-scanning)
    - [Сканирование поддоменов](#find-subdomain)
    - Исследование видимого контента
    - [Поиск скрытого контента (директорий, файлов)](#find-hidden-content)
    - [Определение платформы и веб-окружения](#platform-detection)
    - Определение форм ввода
- [**2. Контроль доступа**](#2)
    - Проверка средств аутентификации и авторизации
    - Определение требований парольной политики
    - Тестирование подбора учетных данных
    - Тестирование восстановления учетной записи
    - Тестирование функций сохранения сессии
    - Тестирование функций идентификации учетной записи
    - Проверка полномочий и прав доступа
    - Исследования сессии (время жизни, сессионный токены, признаки, попытки одновременной работы и т.д.)
    - Проверка CSRF
- [**3. Фаззинг параметров**](#3)
    - [Тестирование приложения к различному виду инъекций (OS, Code, SQL, SOAP, LDAP, XPATH и т.д.)](#injection-testing)
    - [Тестирование приложения к XSS-уязвимостям](#xss-injection-testing)
    - [Проверка тимплейт-инъекций](#tpl-testing)
    - [Проверка к внедрению XML-сущностей](#xxe-testing)
    - [Проверка HTTP заголовков](#http-headers-check)
    - Проверка редиректов и переадресаций
    - Проверка локального и удаленного инклуда
    - Проверка взаимодествия веб-сокетов
- [**4. Проверки логики работы веб-приложения**](#4)
    - Тестирование логики работы приложения на стороне клиента
    - [Тестирования на т.н. «состояние гонки» — race condition](#race-condition-testing)
    - Тестирование канала передачи данных
    - Тестирование доступности информации исходя из прав доступа или его отсутствия
    - Проверка возможности дублирования или разделения данных
- [**5. Проверка серверного окружения**](#5)
    - [Поиск и выявление публичных уязвимостей](#public-server-side-scan)
    - Проверка архитектуры сервера
    - Поиск и выявление публичных уязвимостей
    - Проверка серверных учетных записей (службы и сервисы)
    - Определение настроек сервера или компонентов (SSL и т.д.)
    - Проверка прав доступа

---

## <a name="1"></a>1. Разведка / OSINT

Для того, чтобы понять с чем мы имеем дело, нужно провести поверхностное исследование. Это найти все поддомены, потом проверить везде порты и найти сервисы, которые там запущены, попробовать поискать там скрытые сервисы и скрытые файлы/директории (вспомним про [Security through obscurity](https://en.wikipedia.org/wiki/Security_through_obscurity)).
Ну и да, куда деться без e-mail`ов цели и проверки страниц сотрудников на чувствительные данные? :)

### <a name="ppl-osint"></a>Поиск персонала

- [Lampyre](https://lampyre.io/). Obtain, visualize and analyze data in one place to see what others can’t. Data analysis tool for everyone.
- [Maltego](http://www.paterva.com/web7/) - Proprietary software for open source intelligence and forensics, from Paterva.
- [Intel Techniques](https://inteltechniques.com/menu.html) - Collection of OSINT tools. Menu on the left can be used to navigate through the categories.
- [Intrigue](https://intrigue.io/) is an automated OSINT & Attack Surface discovery framework with powerful API, UI and CLI.
- [theHarvester](https://github.com/laramies/theHarvester) is an E-mail, subdomain and people names harvester.
- [gOSINT](https://github.com/Nhoya/gOSINT) is a multiplatform OSINT Swiss army knife in Golang.
- [A geolocation OSINT tool](https://github.com/ilektrojohn/creepy). Offers geolocation information gathering through social networking platforms.

### <a name="port-scanning"></a>Сканирование портов

Все скатится к тому, что вам нужно будет запустить [Nmap](https://nmap.org/) :) Поэтому вот разные читщиты:

- [https://highon.coffee/blog/nmap-cheat-sheet/](https://highon.coffee/blog/nmap-cheat-sheet/), 
- [https://www.secjuice.com/port-scanning-penetration-testing-part-three/](https://www.secjuice.com/port-scanning-penetration-testing-part-three/)

### <a name="find-subdomain"></a>Сканирование поддоменов

- [Aquatone](https://github.com/michenriksen/aquatone) is a tool for visual inspection of websites across a large amount of hosts and is convenient for quickly gaining an overview of HTTP-based attack surface.
- [DataSploit](https://github.com/upgoingstar/datasploit) is a OSINT visualizer utilizing Shodan, Censys, Clearbit, EmailHunter, FullContact, and Zoomeye behind the scenes.]
- [Sublist3r](https://github.com/aboul3la/Sublist3r) is a python tool designed to enumerate subdomains of websites using OSINT.
- [Gobuster `DNS subdomains`](https://github.com/OJ/gobuster#dns-mode-help). Directory/File, DNS and VHost busting tool written in Go

### <a name="find-hidden-content"></a>Поиск скрытого контента (директорий, файлов)

- [dirsearch](https://github.com/maurosoria/dirsearch) is a simple command line tool designed to brute force directories and files in websites.
- [Gobuster `Directory/File`](https://github.com/OJ/gobuster#dns-mode-help). Directory/File, DNS and VHost busting tool written in Go
- [dirb](https://tools.kali.org/web-applications/dirb) is a Web Content Scanner. It looks for existing (and/or hidden) Web Objects. It basically works by launching a dictionary based attack against a web server and analyzing the response.

### <a name="platform-detection"></a>Определение платформы и веб-окружения

- [What CMS](https://whatcms.org/) or [what runs](https://www.whatruns.com/).
- [CMSmap](https://github.com/Dionach/CMSmap) is a python open source CMS scanner that automates the process of detecting security flaws of the most popular CMSs.
- [WPScan](https://wpscan.org/) is a free, for non-commercial use, black box WordPress vulnerability scanner written for security professionals and blog maintainers to test the security of their sites.
- [JoomScan](https://github.com/rezasp/joomscan) (OWASP Joomla! Vulnerability Scanner) is an open source project, developed with the aim of automating the task of vulnerability detection and reliability assurance in Joomla CMS deployments.

---

## <a name="2"></a>2. Контроль доступа

Окей, нашли сервисы. Ищем теперь польщователей, логины, думаем как что можем сдампить или стащить.

А может быть где-то и нет авторизации или вообще стоит `admin:admin`.

---

## <a name="3"></a>3. Фаззинг параметров

Вообще с фаззингом и тестированием параметров хорошо справится [Burp Suite](https://portswigger.net/burp/) и [The OWASP Zed Attack Proxy (ZAP)](https://github.com/zaproxy/zaproxy). Суем во все поля кавычки, загружаем битые файлы
Но не все можно протестить ими, да и вообще фаззинг это не так-то просто (почитать вкратце про фаззаинг можно [тут](https://dc20e6.ru/announcement/2019/03/15/0x04-workshop-fuzzing.html))

Различные списки для тестирования параметров:

- [PayloadsAllTheThings](https://github.com/swisskyrepo/PayloadsAllTheThings) A list of useful payloads and bypass for Web Application Security and Pentest/CTF
- [The Big List of Naughty Strings](https://github.com/minimaxir/big-list-of-naughty-strings) is a list of strings which have a high probability of causing issues when used as user-input data.

### <a name="http-headers-check"></a>Проверка HTTP заголовков

- [The Mozilla Observatory](https://observatory.mozilla.org/) is a project designed to help developers, system administrators, and security professionals configure their sites safely and securely
- [securityheaders](https://securityheaders.com/) Analyse your HTTP response headers

---

## <a name="4"></a>4. Проверки логики работы веб-приложения

Вообще бизнес логику автоматически проверить крайне сложно. Сделать руками вы это можете при помощи [Burp Suite](https://portswigger.net/burp/) и [The OWASP Zed Attack Proxy (ZAP)](https://github.com/zaproxy/zaproxy).

### <a name="race-condition-testing"></a>Тестирования на т.н. «состояние гонки» — race condition

- [RacePWN](https://github.com/racepwn/racepwn) is a librace library and a racepwn utility that are designed to test a race condition attack through protocols that use a TCP connection.

---

## <a name="5"></a>5. Проверка серверного окружения

### <a name="public-server-side-scan"></a>Поиск и выявление публичных уязвимостей

- [nmap-vulners](https://github.com/vulnersCom/nmap-vulners) NSE script using some well-known service to provide info on vulnerabilities



















