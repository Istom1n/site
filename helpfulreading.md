---
layout: default
permalink: helpfulreading/
excerpt: На этой странице список различных блогов, форумов и телеграм каналов для вечернего чтива 
---

# Helpful stuff

Меню:

 - [Telegram](#telegram)
 - [Форумы](#forums)
 - [Блоги](#blogs)

## <a name="telegram"></a>Telegram чаты и каналы

 - [Новостной агрегатор от DC20e6](https://t.me/defcon_news) Агрегатор новостей из мира информационной безопасности (кучи разных мест). Да, тут огромное количество сообщений постоянно :). [Список RSS](https://dc20e6.ru/files/defcon_news.txt).
 - [Кавычка](https://t.me/webpwn) Уязвимости и атаки на веб-приложения.
 - [Freedom Fox](https://t.me/freedom_fox) information must flow free, money kill it

## <a name="blogs"></a>Блоги

- [bo0om - Explosive blog](https://bo0om.ru/en/) keywords: кавычки и веселье
- [Raz0r.name](http://raz0r.name/) - Web Application Security
- [Бизнес без опасности](https://lukatsky.blogspot.com/) Бумажная безопасность или "Бизнес безопасности или безопасность бизнеса? О том и о другом в одном блоге..."
- [Misc](https://kaimi.io/) - Misc development

## <a name="forums"></a>Форумы

Пачка действующих форумов, затрагивающих тему информационной безопасности [https://link-base.org/](https://link-base.org/), отсортированная по странам.

Отдельно хочется выделить:
- [R0 CREW](https://forum.reverse4you.org/)
- [antichat](http://forum.antichat.ru) Старый-добрый античат, работает с 2004 года	
- [rdot.org](https://rdot.org/forum/) keywords: аспекты несанкционированного доступа, компьютерная безопасность, уязвимости, администрирование, программирование.
- [BugTraq/forum](http://bugtraq.ru/forum/) Общетематический форум портала BugTraq, посвящённый ИБ
- [codeby.net](https://codeby.net/) Информационная безопасность и защита информации компьютерных сетей. Программирование, этичный хакинг и тестирование на проникновение, форензика.
